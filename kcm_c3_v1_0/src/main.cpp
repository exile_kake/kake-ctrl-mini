
#include <Arduino.h>

#include <SPI.h>
#include <oled.h>
#include <oled.h>

// #include <U8g2lib.h>

// #ifdef U8X8_HAVE_HW_SPI
// #include <SPI.h>
// #endif
// #ifdef U8X8_HAVE_HW_I2C
// #include <Wire.h>
// #endif

// #include <esp_now.h>
// #include "WiFi.h"

#include "kkgui.h"
#include "kkgui_cfg.h"
#include "kkgui_app.h"

OLED_SSD1306_SPI DIS(&SPI,5,4,-1); // oled_spi, res, dc, cs

// U8G2_SSD1306_128X64_NONAME_F_4W_SW_SPI u8g2(U8G2_R0, /* clock=*/ 7, /* data=*/ 6, /* cs=*/ -1, /* dc=*/ 4, /* reset=*/ 5);

void oled_refresh()
{
    DIS.Refresh();
    // u8g2.sendBuffer();
}

void oled_setlight(uint8_t i)
{
    DIS.Brightness(i);
}

void oled_setturn(uint8_t i)
{
    DIS.ColorTurn(i);
}

void KKgui_log(const char *fmt, ...)
{
    char pcOutBuf[256];
    va_list args;

    memset(pcOutBuf,0,256);
    va_start(args, fmt);
    vsnprintf(pcOutBuf,256,fmt,args);
    va_end(args);
    Serial.printf(pcOutBuf);
}

extern uint8_t _OLED_GRAM[8][128];

extern int demo_menu_num;
extern kkui_menu_info_s* demo_menu[];
extern kkui_key_cfg_s key_info[];

typedef struct  {
  int direction;
  int speed;
  uint8_t forw_led;
  uint8_t back_led;
} recive_info;

extern recive_info car_data;
uint8_t broadcastAddress[] = {0x48, 0x31, 0xB7, 0x36, 0x0E, 0xD4};

// void OnDataSent(const uint8_t *mac_addr, esp_now_send_status_t status)
// {
//     KKgui_log(status == ESP_NOW_SEND_SUCCESS ? "Delivery Success" : "Delivery Fail");
// }

void esp_now_send_cardata(void)
{
                
    // esp_err_t result = esp_now_send(broadcastAddress, (uint8_t *) &car_data, sizeof(car_data));

    // //判断是否发送成功
    // if (result == ESP_OK) {
    //     KKgui_log("Sent with success");
    // }
    // else {
    //     KKgui_log("Error sending the data");
    // }
}

void setup(void) {
    Serial.begin(115200);

    // Serial.printf("中文测试");

    SPI.begin(7, -1, 6);// /* clock=*/ 7, /* data=*/ 6,
    SPI.setBitOrder(MSBFIRST);
    SPI.setFrequency(40000000);
    SPI.setDataMode(SPI_MODE0);
    DIS.Init();
    DIS.Clear(0);

        
    // WiFi.mode(WIFI_STA);

    // //  初始化ESP-NOW
    // if (esp_now_init() != ESP_OK) 
    // {
    //     KKgui_log("Error initializing ESP-NOW");
    //     return;
    // }
    // esp_now_register_send_cb(OnDataSent);
    // esp_now_peer_info_t peerInfo;
    // memcpy(peerInfo.peer_addr, broadcastAddress, 6);
    // peerInfo.channel = 0;  //通道
    // peerInfo.encrypt = false;//是否加密为False
    // if (esp_now_add_peer(&peerInfo) != ESP_OK){
    //     KKgui_log("Failed to add peer");
    //     return;
    // }

    // u8g2.begin();
    // u8g2.getBufferPtr();
    // Serial.print("u8g2.getBufferTileHeight() : ");
    // Serial.println(u8g2.getBufferTileHeight());
    // Serial.print("u8g2.getBufferTileWidth() : ");
    // Serial.println(u8g2.getBufferTileWidth());

    pinMode(0, INPUT_PULLUP);
    pinMode(1, INPUT_PULLUP);
    pinMode(2, INPUT_PULLUP);
    pinMode(18, INPUT_PULLUP);
    pinMode(19, INPUT_PULLUP);
    pinMode(8, INPUT_PULLUP);
    pinMode(9, INPUT_PULLUP);

    uiDisInit(oled_refresh);
    uiGramInit(_OLED_GRAM);
    // uiGramInit((uint8_t (*)[128])u8g2.getBufferPtr());
    Serial.println("ui init ok");

    uiMenuInit(demo_menu, demo_menu_num);
    uiKeyInit(key_info, 7);
    uiDisClear(0);
}


void loop(void) {
    uiHeartBeat(10);
    delay(8);
}