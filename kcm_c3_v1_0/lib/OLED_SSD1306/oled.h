#ifndef __OLED_H
#define __OLED_H 

#include "Arduino.h"
#include "stdlib.h"	

#include "SPI.h"

#define OLED_CMD  0	//写命令
#define OLED_DATA 1	//写数据

class OLED_SSD1306_SPI{
    public:
        OLED_SSD1306_SPI(SPIClass * oled_spi, int8_t res, int8_t dc, int8_t cs);
        void ClearPoint(uint8_t x,uint8_t y);
        void ColorTurn(uint8_t i);
        void DisplayTurn(uint8_t i);
        void Brightness(uint8_t i);
        void DisPlay_On(void);
        void DisPlay_Off(void);
        void Refresh(void);
        void Clear(uint8_t color);
        void DrawPoint(uint8_t x,uint8_t y,uint8_t t);
        void DrawLine(uint8_t x1,uint8_t y1,uint8_t x2,uint8_t y2,uint8_t mode = 1);
        void DrawCircle(uint8_t x,uint8_t y,uint8_t r);
        void DrawBox(uint8_t x,uint8_t y,uint8_t w, uint8_t h, uint8_t mode = 1);
        void ShowChar(uint8_t x,uint8_t y,uint8_t chr,uint8_t size1,uint8_t mode = 1);
        void ShowString(uint8_t x,uint8_t y,uint8_t *chr,uint8_t size1,uint8_t mode = 1);
        void ShowNum(uint8_t x,uint8_t y,uint32_t num,uint8_t len,uint8_t size1,uint8_t mode = 1);
        void ShowSMG(uint8_t x,uint8_t y,uint32_t num,uint8_t mode = 1);
        void ShowTime(uint8_t x,uint8_t y,uint16_t shi,uint16_t fen,uint16_t miao);
        void ShowChinese(uint8_t x,uint8_t y,uint8_t num,uint8_t size1,uint8_t mode = 1);
        void ShowPicture(uint8_t x,uint8_t y,uint8_t sizex,uint8_t sizey,uint8_t BMP[],uint8_t mode = 1);
        void ShowSymbol(uint8_t x,uint8_t y,uint8_t num,uint8_t mode = 1);
        void Init(void);
    private:
        uint8_t _OLED_GRAM_RB[144];
        SPIClass * _oled_spi;
        int8_t _res;
        int8_t _dc;
        int8_t _cs;
        void Send_Byte(uint8_t dat);
        void WR_Byte(uint8_t dat,uint8_t mode);
};

#endif

