#ifndef __WIFI_SETTING_H__
#define __WIFI_SETTING_H__

#include "kkgui_menu.h"

#ifdef __cplusplus
extern "C" {
#endif

void func_wifi_key(kkui_menu_event_e event, char* tab_name, void* param);
void func_wifi_scan(kkui_menu_event_e event, char* tab_name, void* param);
void func_wifi_info(kkui_menu_event_e event, char* tab_name, void* param);


#ifdef __cplusplus
}
#endif

#endif

