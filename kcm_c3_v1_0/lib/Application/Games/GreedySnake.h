#ifndef __GREEDY_SNAKE_H__
#define __GREEDY_SNAKE_H__

#include "kkgui_menu.h"

#ifdef __cplusplus
extern "C" {
#endif

void Snake_Home_Loop(kkui_menu_event_e event, char* tab_name, void* param);
void Snake_Set_Mode(kkui_menu_event_e event, char* tab_name, void* param);
void Snake_Set_Level(kkui_menu_event_e event, char* tab_name, void* param);
void Snake_Game_Loop(kkui_menu_event_e event, char* tab_name, void* param);
void Snake_Record_Loop(kkui_menu_event_e event, char* tab_name, void* param);

#ifdef __cplusplus
}
#endif

#endif
