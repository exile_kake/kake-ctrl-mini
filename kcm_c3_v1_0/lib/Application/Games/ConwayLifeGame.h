#ifndef __CONWAY_LIFE_GAME_H__
#define __CONWAY_LIFE_GAME_H__

#include "kkgui_menu.h"

#ifdef __cplusplus
extern "C" {
#endif

void KWG_Loop(kkui_menu_event_e event, char* tab_name, void* param);

#ifdef __cplusplus
}
#endif

#endif
