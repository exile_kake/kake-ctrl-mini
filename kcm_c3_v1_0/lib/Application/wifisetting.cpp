/*
 * @Author: wangtianyu wangtianyu@lierda.com
 * @Date: 2023-11-28 09:21:51
 * @LastEditors: wangtianyu wangtianyu@lierda.com
 * @LastEditTime: 2024-01-02 09:34:42
 * @FilePath: \kcm_c3_v1_0\lib\Application\wifisetting.cpp
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */

#include "wifisetting.h"

#include "kkgui_base.h"
#include "kkgui_app.h"
#include "kkgui_virt_keyboard.h"

#include "WiFi.h"

#define WIFI_DIS_MAX        28
#define WIFI_SSID_LEN       30
#define WIFI_RSSI_LEN       30
#define WIFI_PASSWORD_LEN   30

char Wifi_Connect_buf[32] = "未连接";
char Wifi_Connect_name[32] = "-";
uint8_t wifi_onoff = 0;

typedef struct 
{
    char DIS_NAME[20];
    char SSID[WIFI_SSID_LEN + 1];
    char RSSI[WIFI_RSSI_LEN + 1];
    char PASSWORD[WIFI_PASSWORD_LEN + 1];
    wifi_auth_mode_t mode;
}wifi_info_t;

wifi_info_t wifi_info[WIFI_DIS_MAX] = {
    {
        .PASSWORD = {'1','5','9','a','d','g','A','D','G','1','5','9'},
    },
};

char Wifi_SSID[WIFI_DIS_MAX][30];
char Wifi_RSSI[WIFI_DIS_MAX][30];

extern kkui_menu_info_s wifi_menu;
extern kkui_menutab_info_s wifi_menutab[];
// kkui_menutab_info_s wifi_menutab[23]={
//     {(char *)"WIFI开关", &wifi_onoff, SINGLE_CHOICE, func_wifi_key, (void *)&wifi_onoff, NULL, 0},
//     {(char *)Wifi_Connect_buf, (char *)"-", ACTION_EXE_ONCE, func_wifi_info, NULL, NULL, 0},
//     {(char *)"扫描WIFI", (char *)Wifi_Scan_buf, ACTION_EXE_ONCE, func_wifi_scan, NULL, NULL, 0},
// };
// kkui_menu_info_s wifi_menu = {.menu_type = LIST_STRING,.tab_num = 3,.member = wifi_menutab};

void func_wifi_connect(kkui_menu_event_e event, char* tab_name, void* param);

void func_wifi_key(kkui_menu_event_e event, char* tab_name, void* param)
{
    uint8_t wifi_flag = *(uint8_t*)param;
    if(event == ENTER_TAG)
    {
        if(wifi_flag)
        {
            KKgui_log("WIFI OPEN");
            WiFi.mode(WIFI_STA);
            WiFi.setSleep(false);
        }
        else
        {
            KKgui_log("WIFI CLOSE");
            wifi_menu.tab_num = 3;
            WiFi.disconnect();
        }
    }
}

void func_wifi_info(kkui_menu_event_e event, char* tab_name, void* param)
{

}

uint8_t wifi_scan_state = 0;
int wifi_scan_time = 0;
void func_wifi_scan(kkui_menu_event_e event, char* tab_name, void* param)
{
    if(event == ENTER_TAG)
    {
        if(wifi_onoff)
        {
            wifi_scan_state = 0;
            wifi_scan_time = 0;
            WiFi.scanNetworks(true);
        }
        else
        {
            wifi_scan_state = 0;
            wifi_scan_time = 0;
        }
    }
    if(event == LOOP_RUN)
    {
        if(wifi_onoff)
        {
            if(wifi_scan_state == 0)
            {
                if(wifi_scan_time % 160 >= 0 && wifi_scan_time % 160 < 40)
                    uiShowString(36, 25, (char *)"扫描中   ", 12, 1);
                if(wifi_scan_time % 160 >= 40 && wifi_scan_time % 160 < 80)
                    uiShowString(36, 25, (char *)"扫描中.  ", 12, 1);
                if(wifi_scan_time % 160 >= 80 && wifi_scan_time % 160 < 120)
                    uiShowString(36, 25, (char *)"扫描中.. ", 12, 1);
                if(wifi_scan_time % 160 >= 120 && wifi_scan_time % 160 < 160)
                    uiShowString(36, 25, (char *)"扫描中...", 12, 1);

                int n = WiFi.scanComplete(); //获取扫描状态
                // 扫描完成
                if (n >= 0)
                {
                    KKgui_log("%d", n);
                    KKgui_log(" networks found");
                    int i;
                    for (i = 0; (i < n && i < WIFI_DIS_MAX); i ++)
                    {
                        memset(wifi_info[i].SSID, 0, WIFI_SSID_LEN + 1);
                        strncpy(wifi_info[i].SSID, WiFi.SSID(i).c_str(), WIFI_SSID_LEN);
                        KKgui_log("ssid:%d:%s", i, wifi_info[i].SSID);

                        memset(wifi_info[i].RSSI, 0, WIFI_RSSI_LEN + 1);
                        snprintf(wifi_info[i].RSSI, WIFI_RSSI_LEN, "%d", WiFi.RSSI(i));
                        KKgui_log("rssi:%d:%s", i, wifi_info[i].RSSI);

                        wifi_info[i].mode = WiFi.encryptionType(i);

                        if(strlen(wifi_info[i].SSID) <= 15)
                        {
                            strcpy(wifi_info[i].DIS_NAME, wifi_info[i].SSID);
                        }
                        else
                        {
                            strncpy(wifi_info[i].DIS_NAME, wifi_info[i].SSID, 12);
                            strncpy(wifi_info[i].DIS_NAME + 12, "...", 3);
                        }
                        
                        wifi_menutab[3+i].name = wifi_info[i].DIS_NAME;
                        wifi_menutab[3+i].other_dis = wifi_info[i].RSSI;
                        wifi_menutab[3+i].menu_func = func_wifi_connect;
                        wifi_menutab[3+i].param = &wifi_info[i];
                        wifi_menutab[3+i].sub_menu_addr = NULL;
                        wifi_menutab[3+i].type = ACTION_EXE_LOOP;
                    }
                    wifi_menu.tab_num = 3 + i;
                    KKgui_log("%d", i);
                    KKgui_log(" networks dis");

                    wifi_scan_time = 0;
                    wifi_scan_state = 1;
                }
                else
                {
                    wifi_scan_time ++;
                    if(wifi_scan_time > 2000)
                    {
                        wifi_scan_time = 0;
                        wifi_scan_state = 2;
                    }
                }
            }
            else if(wifi_scan_state == 1)
            {
                uiShowString(33, 25, (char *)" 扫描完成 ", 12, 1);
                wifi_scan_time ++;
                if(wifi_scan_time > 80)
                {
                    wifi_scan_state = 3;
                }
            }
            else if(wifi_scan_state == 2)
            {
                uiShowString(33, 25, (char *)" 扫描失败 ", 12, 1);
                wifi_scan_time ++;
                if(wifi_scan_time > 80)
                {
                    wifi_scan_state = 3;
                }
            }
            else if(wifi_scan_state == 3)
            {
                uiBackAppLoop();
            }
            else
            {
                uiBackAppLoop();
            }
            
        }
        else
        {
            if(wifi_scan_state == 0)
            {
                uiShowString(30, 25, (char *)"WIFI未开启", 12, 1);
                wifi_scan_time ++;
                if(wifi_scan_time > 80)
                {
                    wifi_scan_state = 1;
                }
            }
            else
            {
                uiBackAppLoop();
            }
        }
    }
}


uint8_t wifi_connect_state = 0;
int wifi_connect_time = 0;
void func_wifi_connect(kkui_menu_event_e event, char* tab_name, void* param)
{
    wifi_info_t* tab_wifi_info = (wifi_info_t*)param;
    if(event == ENTER_TAG)
    {
        wifi_connect_state = 0;
        wifi_connect_time = 0;
        uiDisClear(0);
    }
    if(event == LOOP_RUN)
    {
        uiDisClear(0);

        if(wifi_connect_state == 0)
        {
            uiShowString(0, 2, (char *)"名称:", 12, 1);
            uiShowString(30, 2, tab_wifi_info->DIS_NAME, 12, 1);
            // uiDrawHLine(0, 15, 128, 1);

            uiShowString(0, 17, (char *)"密码:", 12, 1);
            uiShowString(30, 17, tab_wifi_info->PASSWORD, 12, 1);
            // uiDrawHLine(0, 30, 128, 1);
            
            uiShowString(0, 32, (char *)"加密方式:", 12, 1);
            switch(tab_wifi_info->mode)
            {
                case WIFI_AUTH_OPEN:
                    uiShowString(54, 32, (char*)"OPEN", 12, 1);
                    break;
                case WIFI_AUTH_WEP:
                    uiShowString(54, 32, (char*)"WEP", 12, 1);
                    break;
                case WIFI_AUTH_WPA_PSK:
                    uiShowString(54, 32, (char*)"WPA", 12, 1);
                    break;
                case WIFI_AUTH_WPA2_PSK:
                    uiShowString(54, 32, (char*)"WPA2", 12, 1);
                    break;
                case WIFI_AUTH_WPA_WPA2_PSK:
                    uiShowString(54, 32, (char*)"WPA/WPA2", 12, 1);
                    break;
                case WIFI_AUTH_WPA2_ENTERPRISE:
                    uiShowString(54, 32, (char*)"WPA2 ENTERPRISE", 12, 1);
                    break;
                case WIFI_AUTH_WPA3_PSK:
                    uiShowString(54, 32, (char*)"WPA3", 12, 1);
                    break;
                case WIFI_AUTH_WPA2_WPA3_PSK:
                    uiShowString(54, 32, (char*)"WPA2/WPA3", 12, 1);
                    break;
                case WIFI_AUTH_WAPI_PSK:
                    uiShowString(54, 32, (char*)"WAPI", 12, 1);
                    break;
            }

            uiShowString(0, 52, (char *)"密码", 12, 1);
            uiShowString(52, 52, (char *)"连接", 12, 1);
            uiShowString(104, 52, (char *)"返回", 12, 1);

            if(uiKeyGetValue(UI_KEY_ENTER_ID) == DOWN)
            {
                wifi_connect_time = 0;
                wifi_connect_state = 1;
            }

            if(uiKeyGetValue(UI_KEY_SET_ID) == DOWN)
            {
                uiVirtKeyboard(tab_wifi_info->PASSWORD, "WIFI PASSWORD", WIFI_PASSWORD_LEN);
            }

            if(uiKeyGetValue(UI_KEY_BACK_ID) == DOWN)
            {
                uiBackAppLoop();
            }
        }
        else if(wifi_connect_state == 1) // 启动连接
        {
            if ((strlen(tab_wifi_info->PASSWORD) >= 8 && tab_wifi_info->mode != WIFI_AUTH_OPEN) || (strlen(tab_wifi_info->PASSWORD) == 0 && tab_wifi_info->mode == WIFI_AUTH_OPEN))
            {
                WiFi.begin(tab_wifi_info->DIS_NAME, tab_wifi_info->PASSWORD);
                wifi_connect_state = 2;
                wifi_connect_time = 0;
            }
            else
            {
                wifi_connect_state = 4;
                wifi_connect_time = 0;
            }
        }
        else if(wifi_connect_state == 2) // 连接状态检测
        {
            if(wifi_connect_time % 160 >= 0 && wifi_connect_time % 160 < 40)
                uiShowString(36, 25, (char *)"连接中   ", 12, 1);
            if(wifi_connect_time % 160 >= 40 && wifi_connect_time % 160 < 80)
                uiShowString(36, 25, (char *)"连接中.  ", 12, 1);
            if(wifi_connect_time % 160 >= 80 && wifi_connect_time % 160 < 120)
                uiShowString(36, 25, (char *)"连接中.. ", 12, 1);
            if(wifi_connect_time % 160 >= 120 && wifi_connect_time % 160 < 160)
                uiShowString(36, 25, (char *)"连接中...", 12, 1);
            if(WiFi.status() != WL_CONNECTED)
            {			
                wifi_connect_time ++;
                if(wifi_connect_time > 2000)
                {
                    wifi_connect_time = 0;
                    wifi_connect_state = 3;
                }
            }
            if(WiFi.status() == WL_CONNECT_FAILED)
            {
                wifi_connect_time = 0;
                wifi_connect_state = 3;
            }
            if(WiFi.status() == WL_CONNECTED)
            {
                wifi_connect_time = 0;
                wifi_connect_state = 3;
                strcpy(Wifi_Connect_buf, "已连接");
                strcpy(Wifi_Connect_name, tab_wifi_info->DIS_NAME);
            }
        }
        else if(wifi_connect_state == 3) // 连接结果
        {        
            if(WiFi.status()==WL_CONNECTED)  
            {
                uiShowString(33, 25, (char *)" 连接成功 ", 12, 1);
                wifi_connect_time ++;
                if(wifi_connect_time > 80)
                {
                    wifi_connect_state = 0;
                    uiBackAppLoop();
                }
            }  
            else if(WiFi.status()==WL_CONNECT_FAILED)  
            {
                uiShowString(33, 25, (char *)" 连接失败 ", 12, 1);
                wifi_connect_time ++;
                if(wifi_connect_time > 80)
                {
                    wifi_connect_state = 0;
                    wifi_connect_time = 0;
                }
            }  
            else
            {
                uiShowString(33, 25, (char *)" 连接超时 ", 12, 1);
                wifi_connect_time ++;
                if(wifi_connect_time > 80)
                {
                    wifi_connect_state = 0;
                    wifi_connect_time = 0;
                }
            }  
        }
        else if(wifi_connect_state == 4) // 密码错误
        {        
            uiShowString(33, 25, (char *)" 密码错误 ", 12, 1);
            wifi_connect_time ++;
            if(wifi_connect_time > 80)
            {
                wifi_connect_state = 0;
                wifi_connect_time = 0;
            }
        }
    }
}