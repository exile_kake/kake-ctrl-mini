#ifndef __TCP_TEST_H__
#define __TCP_TEST_H__

#include "kkgui_menu.h"

#ifdef __cplusplus
extern "C" {
#endif

void tcp_client_ip_set(kkui_menu_event_e event, char* tab_name, void* param);
void tcp_client_port_set(kkui_menu_event_e event, char* tab_name, void* param);
void tcp_client_loop(kkui_menu_event_e event, char* tab_name, void* param);


#ifdef __cplusplus
}
#endif

#endif

