#include "kkgui_virt_keyboard.h"

#include "kkgui_base.h"
#include "kkgui_cfg.h"
#include "kkgui_app.h"

#include "stdio.h"
#include "string.h"
/* =============================== */
/* define */



/* =============================== */
/* typedef */



/* =============================== */
/* variable statement*/
extern uiRefershCB uiRefresh;


/* =============================== */
/* variable definition*/



/* =============================== */
/* function statement*/



/* =============================== */
/* function definition*/

#define KOK  0xf0  // OK
#define BAK  0xf1  // back
#define RTN  0xf2  // return
#define FON  0xf3  // Case sensitivity
#define CAS  0xf4  // symbol

static uint8_t uiCursorMode = 1; // 1 : keyboard cursor or 2 : Text cursor
static uint8_t uiKeysetPage = 0;
static uint8_t uiKeysetCaps = 0;
static uint8_t uiKeysetString = 0;
static uint8_t uiKeysetCursorX = 0; // keyboard cursor
static uint8_t uiKeysetCursorY = 0;
static int16_t uiTextCursor = 0; // Text cursor
static uint16_t uiTextLen = 0; // Text cursor
static int16_t uiTextDisPos = 0; // Text cursor
static uint16_t uiDataMaxLen = 0;
static uint8_t uiReturnFlag = 0;
static char* uiSaveBuffer = NULL;

const static uint8_t icon_return[] =
{0x00,0x04,0x4E,0x5F,0x44,0x44,0x38,0x00}; // 88
const static uint8_t icon_back[] =
{0x00,0x00,0x20,0x30,0x38,0x24,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x00,0x00,0x00}; // 816
const static uint8_t icon_space_big[] =
{0x00,0x60,0x60,0x40,0x40,0x40,0x40,0x40,0x40,0x40,0x40,0x40,0x40,0x60,0x60,0x00};
const static uint8_t icon_ok[] =
{0x00,0x00,0x38,0x44,0x44,0x44,0x38,0x00,0x7E,0x10,0x28,0x44,0x00,0x60,0x60,0x00};
const static uint8_t icon_space_small[] =
{0x00,0x60,0x40,0x40,0x40,0x60,0x00,0x00};
const static uint8_t icon_caps[] =
{0x08,0x0C,0x7A,0x41,0x7A,0x0C,0x08,0x00}; // 88
const static uint8_t icon_capslocked[] =
{0x08,0x0C,0x7E,0x7F,0x7E,0x0C,0x08,0x00}; // 88
const static uint8_t icon_string[] =
{0x50,0x30,0x02,0x01,0x51,0x09,0x06,0x00}; // 88

const static uint8_t uiKeyboardContent[3][5][10] =
{
    {
        {'1', '2', '3', '4', '5', '6', '7', '8', '9', '0'},
        {'q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p'},
        {'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', RTN},
        {'z', 'x', 'c', 'v', 'b', 'n', 'm', ',', BAK, BAK},
        {FON, CAS, '"', ' ', ' ', ' ', ' ', '.', KOK, KOK}
    },
    {
        {'1', '2', '3', '4', '5', '6', '7', '8', '9', '0'},
        {'Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P'},
        {'A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', RTN},
        {'Z', 'X', 'C', 'V', 'B', 'N', 'M', '-', BAK, BAK},
        {FON, CAS, '"', ' ', ' ', ' ', ' ', '.', KOK, KOK}
    },
    {
        {'1', '2', '3', '4', '5', '6', '7', '8', '9', '0'},
        {'!', '"', '#', '$', '%', '&', '@', '\'','(', ')'},
        {',', '.', '+', '-', '*', '/', '=', '<', '>', RTN},
        {':', ';', '[', ']', '\\','^', '_', '`', BAK, BAK},
        {FON, CAS, '~', '{', '}', '?', '|', ' ', KOK, KOK}
    },
};

static void uiVirtKeyboardAct(uint8_t *get_data)
{
    if(uiKeyGetValue(UI_KEY_ENTER_ID) == DOWN)
    {
        if(uiKeyboardContent[uiKeysetPage][uiKeysetCursorY][uiKeysetCursorX] == FON) // todo
        {
            if(uiKeysetString)
                uiKeysetString = 0;
            else
                uiKeysetCaps = !uiKeysetCaps;

            if(uiKeysetCaps) 
                uiKeysetPage = 1;
            else 
                uiKeysetPage = 0;
        }
        else if(uiKeyboardContent[uiKeysetPage][uiKeysetCursorY][uiKeysetCursorX] == CAS) // todo
        {
            uiKeysetString = !uiKeysetString;

            if(uiKeysetString)
                uiKeysetPage = 2;
            else
            {
                if(uiKeysetCaps) 
                    uiKeysetPage = 1;
                else 
                    uiKeysetPage = 0;
            }
        }
        else if(uiKeyboardContent[uiKeysetPage][uiKeysetCursorY][uiKeysetCursorX] == BAK) // todo
        {
            if(uiTextCursor > 0)
            {
                if(strlen(uiSaveBuffer + uiTextCursor) > 0)
                {
                    memmove(uiSaveBuffer + uiTextCursor - 1, uiSaveBuffer + uiTextCursor, strlen(uiSaveBuffer + uiTextCursor));
                    uiSaveBuffer[strlen(uiSaveBuffer) - 1] = 0;
                    uiTextCursor --;
                }
                else
                {
                    uiTextCursor --;
                    uiSaveBuffer[uiTextCursor] = 0;
                }
            }
        }
        else if(uiKeyboardContent[uiKeysetPage][uiKeysetCursorY][uiKeysetCursorX] == RTN)
        {
            uiReturnFlag = 1;
        }
        else if(uiKeyboardContent[uiKeysetPage][uiKeysetCursorY][uiKeysetCursorX] == KOK)
        {
            uiReturnFlag = 1;
            memset(get_data, 0, uiDataMaxLen);
            strcpy(get_data, uiSaveBuffer);
        }
        else 
        // if(uiKeyboardContent[uiKeysetPage][uiKeysetCursorY][uiKeysetCursorX] & 0x80 == 0) // todo 超限
        {
            if(strlen(uiSaveBuffer + uiTextCursor) > 0 && uiTextCursor < uiDataMaxLen - 1)
            {
                if(strlen(uiSaveBuffer) >= uiDataMaxLen) // MAX len
                {
                    memmove(uiSaveBuffer + uiTextCursor + 1, uiSaveBuffer + uiTextCursor, strlen(uiSaveBuffer + uiTextCursor) - 1);
                }
                else
                {
                    memmove(uiSaveBuffer + uiTextCursor + 1, uiSaveBuffer + uiTextCursor, strlen(uiSaveBuffer + uiTextCursor));
                }
                uiSaveBuffer[uiTextCursor] = uiKeyboardContent[uiKeysetPage][uiKeysetCursorY][uiKeysetCursorX];
                uiTextCursor ++;
            }
            else
            {
                uiSaveBuffer[uiTextCursor] = uiKeyboardContent[uiKeysetPage][uiKeysetCursorY][uiKeysetCursorX];
                if(strlen(uiSaveBuffer) < uiDataMaxLen)
                {
                    uiTextCursor ++;
                }
            }
        }
    }
    if(uiKeyGetValue(UI_KEY_SET_ID) == DOWN)
    {
        if(uiCursorMode == 1 && (uiKeysetCursorX == 8 || uiKeysetCursorX == 9) && uiKeysetCursorY == 4)
        {
            uiReturnFlag = 1;
            memset(get_data, 0, uiDataMaxLen);
            strcpy(get_data, uiSaveBuffer);
        }
        else
        {
            uiCursorMode = 1;
            uiKeysetCursorX = 9;
            uiKeysetCursorY = 4;
        }
    }   
    if(uiKeyGetValue(UI_KEY_BACK_ID) == DOWN)
    {
        if(uiCursorMode == 1 && uiKeysetCursorX == 9 && uiKeysetCursorY == 2)
        {
            uiReturnFlag = 1;
        }
        else
        {
            uiCursorMode = 1;
            uiKeysetCursorX = 9;
            uiKeysetCursorY = 2;
        }
    }
}

static void uiVirtKeyboardDisTextCur(void)
{
    static uint8_t flictime = 0;
    if(uiTextCursor - uiTextDisPos > 20) //
    {
        uiTextDisPos = uiTextCursor - 20;
    } 
    else if(uiTextCursor - uiTextDisPos < 0)
    {
        uiTextDisPos = uiTextCursor;
    }

    if(uiCursorMode == 1)
    {
        if(flictime < 70)
        {
            uiDrawBox(1 + (uiTextCursor - uiTextDisPos) * 6, 8, 6, 3, 1);
        }
    }
    if(uiCursorMode == 2)
    {
        if(flictime < 80)
        {
            uiDrawBlock(1 + (uiTextCursor - uiTextDisPos) * 6, 8, 6, 3, 1);
        }
    }
    flictime ++;
    if(flictime > 150)
        flictime = 0;
}

#define LINE_SPACE      12
#define COLUMN_SPACE    10
static void uiVirtKeyboardDiskey(void)
{
    uint8_t retx, rety;
    for(rety = 0; rety < 6; rety ++)
    {
        uiDrawHLine(3, 13 + rety * COLUMN_SPACE, 121, 1);
    }
    uiDrawVLine(3 + 10 * LINE_SPACE, 13, 50, 1);
    for(retx = 0; retx < 4; retx ++)
    {
        uiDrawVLine(3 + retx * LINE_SPACE, 13, 50, 1);
    }
    if(uiKeysetPage == 2)
    {
        for(retx = 4; retx < 7; retx ++)
        {
            uiDrawVLine(3 + retx * LINE_SPACE, 13, 50, 1);
        }
    }
    else
    {
        for(retx = 4; retx < 7; retx ++)
        {
            uiDrawVLine(3 + retx * LINE_SPACE, 13, 40, 1);
        }
    }
    for(retx = 7; retx < 9; retx ++)
    {
        uiDrawVLine(3 + retx * LINE_SPACE, 13, 50, 1);
    }
    uiDrawVLine(3 + 9 * LINE_SPACE, 13, 30, 1);
    for(rety = 0; rety < 5; rety ++)
    {
        for(retx = 0; retx < 10; retx ++)
        {
            uiShowChar(6 + LINE_SPACE * retx, 15 + COLUMN_SPACE * rety, uiKeyboardContent[uiKeysetPage][rety][retx], 8, 1);
        }
    }
    if(uiKeysetCaps)
        uiShowPicture(6 + LINE_SPACE * 0, 15 + COLUMN_SPACE * 4, 8, 8, (uint8_t *)icon_capslocked, 1); // capslock
    else
        uiShowPicture(6 + LINE_SPACE * 0, 15 + COLUMN_SPACE * 4, 8, 8, (uint8_t *)icon_caps, 1); // capslock
    uiShowPicture(6 + LINE_SPACE * 1, 15 + COLUMN_SPACE * 4, 8, 8, (uint8_t *)icon_string, 1); // string
    if(uiKeysetString)
        uiShowPicture(6 + LINE_SPACE * 7, 15 + COLUMN_SPACE * 4, 8, 8, (uint8_t *)icon_space_small, 1); // short space
    else
        uiShowPicture(7 + LINE_SPACE * 4, 15 + COLUMN_SPACE * 4, 16, 8, (uint8_t *)icon_space_big, 1); // long space
    uiShowPicture(7 + LINE_SPACE * 8, 15 + COLUMN_SPACE * 3, 16, 8, (uint8_t *)icon_back, 1); // back
    uiShowPicture(8 + LINE_SPACE * 8, 15 + COLUMN_SPACE * 4, 16, 8, (uint8_t *)icon_ok, 1); // ok
    uiShowPicture(6 + LINE_SPACE * 9, 15 + COLUMN_SPACE * 2, 8, 8, (uint8_t *)icon_return, 1); // rtn


    if(uiCursorMode == 1)
    {
        if(uiKeysetCursorX >= 3 && uiKeysetCursorX <= 6 && uiKeysetCursorY == 4 && uiKeysetPage != 2)
            uiDrawBlock(4 + 3 * LINE_SPACE, 14 + 4 * COLUMN_SPACE, 47, 9, 2);
        else if(uiKeysetCursorX >= 8 && uiKeysetCursorX <= 9 && (uiKeysetCursorY == 3 || uiKeysetCursorY == 4))
            uiDrawBlock(4 + 8 * LINE_SPACE, 14 + uiKeysetCursorY * COLUMN_SPACE, 23, 9, 2);
        else
            uiDrawBlock(4 + uiKeysetCursorX * LINE_SPACE, 14 + uiKeysetCursorY * COLUMN_SPACE, 11, 9, 2);
    }
}

static void uiVirtKeyboardCursorAct(void)
{
    if(uiCursorMode == 1)
    {
        if(uiKeyGetValue(UI_KEY_UP_ID) == DOWN)
        {
            if(uiKeysetCursorY > 0)
            {
                uiKeysetCursorY --;
            }
            else
            {
                uiCursorMode = 2;
            }
        }
        if(uiKeyGetValue(UI_KEY_DOWN_ID) == DOWN)
        {
            if(uiKeysetCursorY < 4)
            {
                uiKeysetCursorY ++;
            }
        }
        if(uiKeyGetValue(UI_KEY_LEFT_ID) == DOWN)
        {
            if(uiKeysetCursorX > 0)
            {
                if(uiKeysetCursorX >= 3 && uiKeysetCursorX <= 6 && uiKeysetCursorY == 4 && uiKeysetPage != 2)
                    uiKeysetCursorX = 2;
                else if(uiKeysetCursorX >= 8 && uiKeysetCursorX <= 9 && (uiKeysetCursorY == 3 || uiKeysetCursorY == 4))
                    uiKeysetCursorX = 7;
                else
                    uiKeysetCursorX --;
            }
            else
            {
                uiKeysetCursorX = 9;
            }
        }
        if(uiKeyGetValue(UI_KEY_RIGHT_ID) == DOWN)
        {
            if(uiKeysetCursorX < 9)
            {
                if(uiKeysetCursorX >= 3 && uiKeysetCursorX <= 6 && uiKeysetCursorY == 4 && uiKeysetPage != 2)
                    uiKeysetCursorX = 7;
                else if(uiKeysetCursorX >= 8 && uiKeysetCursorX <= 9 && (uiKeysetCursorY == 3 || uiKeysetCursorY == 4))
                    uiKeysetCursorX = 0;
                else
                    uiKeysetCursorX ++;
            }
            else
            {
                uiKeysetCursorX = 0;
            }
        }
    }
    if(uiCursorMode == 2)
    {
        if(uiKeyGetValue(UI_KEY_DOWN_ID) == DOWN)
        {
            uiCursorMode = 1;
        }
        if(uiKeyGetValue(UI_KEY_LEFT_ID) == DOWN)
        {
            if(uiTextCursor > 0)
            {
                uiTextCursor --;
            }
            else
            {
                if(uiTextLen < uiDataMaxLen)
                    uiTextCursor = uiTextLen;
                else
                    uiTextCursor = uiTextLen - 1;
            }
        }
        if(uiKeyGetValue(UI_KEY_RIGHT_ID) == DOWN)
        {
            if(uiTextCursor < uiTextLen && uiTextCursor < uiDataMaxLen - 1)
            {
                uiTextCursor ++;
            }
            else
            {
                uiTextCursor = 0;
            }
        }
    }
}
/* todo
添加 *显\实显 选项
添加最大输入长度字段,超限处理
修改超过屏幕显示时的处理
*添加双键判断（只有左右没有上下键）
*/
uint8_t uiVirtKeyboard(char *get_data, const char* dis_info, uint16_t max_len)
{
    // uiSaveData();
    uiCursorMode = 1;
    uiKeysetPage = 0;
    uiKeysetCaps = 0;
    uiKeysetString = 0;
    uiKeysetCursorX = 0;
    uiKeysetCursorY = 0;
    uiReturnFlag = 0;
    uiTextLen = strlen(get_data);
    uiTextCursor = uiTextLen;
    uiDataMaxLen = max_len;
    uiSaveBuffer = NULL;
    uiSaveBuffer = malloc(max_len + 1);
    if(uiSaveBuffer == NULL)
    {
        KKgui_log("uiVirtKeyboard Buffer malloc error");
        return 1;
    }
    memset(uiSaveBuffer, 0, uiDataMaxLen + 1);
    strcpy(uiSaveBuffer, get_data);
    uiAllKeyClearValue();
    uiVirtKeyboardDiskey();
    uiDrawBlock(4 + uiKeysetCursorX * 12, 14 + uiKeysetCursorY * 10, 11, 9, 2);
    while(1)
    {
        uiDisClear(0);
        uiKeyScan(5);
        uiTextLen = strlen(uiSaveBuffer);

        if(uiTextLen == 0 && dis_info && strlen(dis_info))
        {
            uiShowString(0, 0, (char *)dis_info, 8, 1);
            uiDisMask4Point(0, 0, 128, 9, 0x02, 0);
        }
        else
        {
            uiShowString(1 - 6 * uiTextDisPos, 0, (char *)uiSaveBuffer, 8, 1);
        }

        uiVirtKeyboardCursorAct();
        uiVirtKeyboardAct(get_data);
        uiVirtKeyboardDiskey();
        uiVirtKeyboardDisTextCur();

        uiRefresh();

        if(uiReturnFlag)
            break;

        uiAllKeyClearValue();

        delay(5);
    }
    uiAllKeyClearValue();
    // uiReductData();

    free(uiSaveBuffer);
    uiSaveBuffer = NULL;
    return 0;
}
