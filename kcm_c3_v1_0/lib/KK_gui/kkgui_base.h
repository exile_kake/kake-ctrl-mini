#ifndef __KKGUI_BASE_H__
#define __KKGUI_BASE_H__

/* =============================== */
/* include */
#include "kkgui_cfg.h"

#ifdef __cplusplus
extern "C" {
#endif

/* =============================== */
/* define */
// 中文同时只能开启一个
#define KKUI_CN_GB2312_FONT12_EN        0
#define KKUI_CN_UNICODE_FONT12_EN       1

#define KKUI_DRAW_UPPER_RIGHT   0x01
#define KKUI_DRAW_UPPER_LEFT    0x02
#define KKUI_DRAW_LOWER_LEFT    0x04
#define KKUI_DRAW_LOWER_RIGHT   0x08
#define KKUI_DRAW_ALL (KKUI_DRAW_UPPER_RIGHT|KKUI_DRAW_UPPER_LEFT|KKUI_DRAW_LOWER_LEFT|KKUI_DRAW_LOWER_RIGHT)

/* =============================== */
/* typedef */



/* =============================== */
/* variable statement*/



/* =============================== */
/* function statement*/
#define uiSetPoint(__x,__y,__t)     _uiSetPoint(__x,__y,__t) 
// #define uiTornPoint(__x,__y)        _uiTornPoint(__x,__y)   

void uiDisClear(uint8_t mode);
void uiClearDisWindows(void);
void uiSetDisWindows(int x, int y, int w, int h);
void uiDrawPoint(int x,int y,uint8_t color);
void uiDisMask4Point(int x, int y, int sizex, int sizey, uint8_t value, uint8_t color);
void uiDrawHLine(int x,int y,int len, uint8_t color);
void uiDrawVLine(int x,int y,int len, uint8_t color);
void uiDrawLine(int x1,int y1,int x2,int y2, uint8_t color);
void uiDrawDisc(int x0, int y0, uint16_t r, uint8_t option, uint8_t color);
void uiDrawCircle(int x0, int y0, uint16_t r, uint8_t option, uint8_t color);
void uiDrawBlock(int x,int y,int w, int h, uint8_t color);
void uiDrawRBlock(int x, int y, uint16_t w, uint16_t h, uint16_t r, uint8_t color);
void uiDrawBox(int x, int y, int w, int h, uint8_t color);
void uiDrawRBox(int x, int y, uint16_t w, uint16_t h, uint16_t r, uint8_t color);
void uiShowChar(int x,int y,char chr,uint8_t size1,uint8_t mode);
#if KKUI_CN_GB2312_FONT12_EN || KKUI_CN_UNICODE_FONT12_EN
uint8_t uiShowChinese(int x, int y, uint8_t* ch, uint8_t size1, uint8_t mode);
#endif
void uiShowString(int x0, int y0, char *chr, uint8_t size1, uint8_t mode);
void uiShowPicture(int x,int y,int sizex,int sizey,uint8_t BMP[],uint8_t mode);



#ifdef __cplusplus
}
#endif

#endif

