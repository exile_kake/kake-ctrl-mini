/*
 * @Author: wangtianyu wangtianyu@lierda.com
 * @Date: 2023-07-05 10:17:59
 * @LastEditors: wangtianyu wangtianyu@lierda.com
 * @LastEditTime: 2024-01-17 18:51:59
 * @FilePath: \kcm_c3_v1_0\lib\KK_gui\kkgui_app.c
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */


#include "kkgui_app.h"

#include "kkgui_key.h"
#include "kkgui_base.h"
#include "kkgui_virt_keyboard.h"

// APP inc
#include "Games/ConwayLifeGame.h"
#include "Games/GreedySnake.h"

#include "Wireless/TcpTest.h"
#include "Wireless/UdpTest.h"
#include "wifisetting.h"
/* =============================== */
/* define */



/* =============================== */
/* typedef */


/* =============================== */
/* function statement*/

// extern void KWG_Loop(kkui_menu_event_e event, char* tab_name, void* param);

// extern void func_wifi_key(kkui_menu_event_e event, char* tab_name, void* param);
// extern void func_wifi_scan(kkui_menu_event_e event, char* tab_name, void* param);
// extern void func_wifi_info(kkui_menu_event_e event, char* tab_name, void* param);

/* =============================== */
/* variable statement*/

/* =============================== */
/* variable definition*/
const uint8_t main_icon_pic[][128] = {
{0xFC,0xFE,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0x3F,0x0F,0x1F,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFE,0xFC,
0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0x1F,0x01,0xC0,0xF8,0x00,0x0F,0xFF,0xFF,0x7F,0x7F,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
0xFF,0xFF,0xCF,0xCF,0xCF,0xCF,0xCF,0xCF,0xCF,0xC0,0xE0,0xFE,0xFF,0xFF,0xC0,0x00,0x3F,0x07,0x80,0xF0,0xE0,0xC7,0xCF,0xCF,0xCF,0xCF,0xCF,0xCF,0xCF,0xCF,0xFF,0xFF,
0x3F,0x7F,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xF0,0xE0,0xF8,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0x7F,0x3F},/*"C:\Users\11412\Desktop\bo.bmp",0*/

{0xFC,0xFE,0xFF,0xFF,0x1F,0x0F,0xC7,0xE7,0xE7,0xE7,0xE7,0xE7,0xE7,0xE7,0xE7,0xE7,0xE7,0xE7,0xE7,0xE7,0xE7,0xE7,0xE7,0xE7,0xE7,0xE7,0xC7,0x0F,0x1F,0xFF,0xFE,0xFC,
0xFF,0xFF,0xFF,0xF9,0x09,0x09,0xF9,0xFF,0xFF,0xFF,0x79,0x79,0x79,0x79,0x79,0x79,0x79,0x79,0x79,0x79,0x79,0x79,0x79,0x79,0xFF,0xFF,0xFF,0x00,0x00,0xFF,0xFF,0xFF,
0xFF,0xFF,0xFF,0x9F,0x90,0x90,0x9F,0xFF,0xFF,0xFF,0x9E,0x9E,0x9E,0x9E,0x9E,0x9E,0x9E,0x9E,0x9E,0x9E,0x9E,0x9E,0x9E,0x9E,0xFF,0xFF,0xFF,0x00,0x00,0xFF,0xFF,0xFF,
0x3F,0x7F,0xFF,0xFF,0xF8,0xF0,0xE3,0xE7,0xE7,0xE7,0xE7,0xE7,0xE7,0xE7,0xE7,0xE7,0xE7,0xE7,0xE7,0xE7,0xE7,0xE7,0xE7,0xE7,0xE7,0xE7,0xE3,0xF0,0xF8,0xFF,0x7F,0x3F},/*"C:\Users\11412\Desktop\note.bmp",0*/

{0xFC,0xFE,0xFF,0xCF,0xC7,0xE7,0x7F,0x3F,0x1F,0x0F,0xCF,0x8F,0x07,0x07,0x07,0xF7,0xF7,0x07,0x07,0x07,0x8F,0xCF,0x0F,0x1F,0x3F,0x7F,0xE7,0xC7,0xCF,0xFF,0xFE,0xFC,
0xFF,0xFF,0xFF,0x1F,0x07,0x01,0x08,0x18,0x10,0x00,0x00,0x03,0x00,0x00,0x80,0x80,0xC0,0xC0,0x60,0x30,0x0B,0x00,0x10,0x10,0x18,0x08,0x00,0x03,0x0F,0xFF,0xFF,0xFF,
0xFF,0xFF,0xFF,0xFE,0xFC,0xFC,0xFC,0xFC,0xFC,0xFC,0xFC,0xDC,0x1C,0x1F,0x7F,0xFF,0xFF,0x7F,0x1F,0x1C,0xDC,0xFC,0xFC,0xFC,0xFC,0xFC,0xFC,0xFC,0xFE,0xFF,0xFF,0xFF,
0x3F,0x7F,0xFF,0xF9,0xF1,0xF3,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFC,0xF0,0xE3,0xF1,0xF8,0xFE,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xF3,0xF1,0xF9,0xFF,0x7F,0x3F},/*"C:\Users\11412\Desktop\vol.bmp",0*/

{0xFC,0xFE,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0x7F,0x3F,0x3F,0x7F,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFE,0xFC,
0xFF,0x7F,0x7F,0x3F,0x3F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7E,0x7C,0x7C,0x7E,0x7E,0x7E,0x7E,0x7C,0x79,0x47,0x3F,0x7F,0x7F,0x7F,0x7F,0x7F,0x3F,0x1F,0x1F,0x3F,0xFF,
0xFF,0xFE,0xFE,0xFC,0xFC,0xFE,0xFE,0xFE,0x3E,0x3E,0x3E,0x3E,0x7E,0x7E,0x7E,0x9E,0xC6,0xF8,0xFE,0xFE,0xFE,0xFE,0xFE,0xFE,0xFE,0xFE,0xFE,0xFC,0xF8,0xF8,0xFC,0xFF,
0x3F,0x7F,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFC,0xFC,0xFC,0xFC,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0x7F,0x3F},/*"C:\Users\11412\Desktop\USB.bmp",0*/

{0xFC,0xFE,0xFF,0xFF,0x7F,0x3F,0x3F,0x3F,0x7F,0x7F,0x3F,0x0F,0x07,0xE3,0xF3,0xF3,0xF3,0xF3,0xE3,0x07,0x0F,0x3F,0x7F,0x7F,0x3F,0x3F,0x3F,0x7F,0xFF,0xFF,0xFE,0xFC,
0xFF,0xFF,0xE7,0xC1,0x88,0x1E,0x3F,0xFF,0xFE,0xFE,0x3E,0x0F,0xC7,0xE7,0xF3,0xF3,0xF3,0xF3,0xE7,0xC7,0x0F,0x3E,0xFE,0xFE,0xFF,0x3F,0x1E,0x88,0xC1,0xE7,0xFF,0xFF,
0xFF,0xFF,0xE7,0x83,0x11,0x78,0xFC,0xFF,0x7F,0x7F,0x7C,0xF0,0xE3,0xE7,0xCF,0xCF,0xCF,0xCF,0xE7,0xE3,0xF0,0x7C,0x7F,0x7F,0xFF,0xFC,0x78,0x11,0x83,0xE7,0xFF,0xFF,
0x3F,0x7F,0xFF,0xFF,0xFE,0xFC,0xFC,0xFC,0xFE,0xFE,0xFC,0xF0,0xE0,0xC7,0xCF,0xCF,0xCF,0xCF,0xC7,0xE0,0xF0,0xFC,0xFE,0xFE,0xFC,0xFC,0xFC,0xFE,0xFF,0xFF,0x7F,0x3F},/*"C:\Users\11412\Desktop\set.bmp",0*/

{0xFC,0xFE,0xFF,0xFF,0xFF,0xFF,0xFF,0x7F,0x3F,0x3F,0x3F,0x1F,0x1F,0x1F,0x1F,0x1F,0x1F,0x1F,0x1F,0x1F,0x1F,0x3F,0x3F,0x3F,0x7F,0xFF,0xFF,0xFF,0xFF,0xFF,0xFE,0xFC,
0xFF,0xFF,0xEF,0xC7,0xC3,0xE1,0xF0,0x78,0x3C,0x1E,0x0E,0x8F,0xC7,0xC7,0xC7,0xC7,0xC7,0xC7,0xC7,0xC7,0x8F,0x0E,0x1E,0x3C,0x78,0xF0,0xE1,0xC3,0xC7,0xEF,0xFF,0xFF,
0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFC,0xFC,0xFC,0xFE,0xEF,0xC7,0xC3,0xE3,0xE1,0x71,0x71,0xE1,0xE3,0xC3,0xC7,0xEF,0xFE,0xFC,0xFC,0xFC,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
0x3F,0x7F,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xF9,0xF0,0xE0,0xE0,0xF0,0xF9,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0x7F,0x3F},/*"C:\Users\11412\Desktop\wifi.bmp",0*/

{0xFC,0xFE,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0x3F,0x1F,0x9F,0xCF,0xCF,0xCF,0xCF,0xCF,0xCF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFE,0xFC,
0xFF,0xFF,0xFF,0xFF,0x7F,0x1F,0x0F,0x07,0x87,0x87,0x87,0x07,0x0F,0x1F,0x1F,0x18,0x18,0x1F,0x1F,0x0F,0x0F,0x07,0x07,0x07,0x07,0x0F,0x1F,0x7F,0xFF,0xFF,0xFF,0xFF,
0xFF,0xFF,0xFF,0x03,0x00,0x00,0x0E,0x0E,0x3F,0x3F,0x3F,0x0E,0x0E,0x00,0x80,0x80,0x80,0x80,0x00,0x00,0x1E,0x3F,0x3F,0x3F,0x3F,0x1E,0x00,0x00,0x03,0xFF,0xFF,0xFF,
0x3F,0x7F,0xFF,0xF8,0xF0,0xF8,0xF8,0xF8,0xFC,0xFE,0xFE,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFE,0xFE,0xFC,0xFC,0xF8,0xF8,0xF0,0xF8,0xFF,0x7F,0x3F},/*"C:\Users\11412\Desktop\game.bmp",0*/
/* (32 X 32 )*/

};

// 各个菜单的信息，作为uiMenuInit的输入参数

// 波形菜单子内容
    uint8_t teat_a1 = 1;
    kkui_menutab_info_s demo_menutab_A1[]={
        {(char *)"开关", &teat_a1, SINGLE_CHOICE, NULL, NULL, &teat_a1},
        {(char *)"A12", (char *)"a12", ACTION_EXE_ONCE, NULL, NULL, NULL},
        {(char *)"A13", NULL, ACTION_EXE_ONCE, NULL, NULL, NULL},
    };
    kkui_menu_info_s demo_menu_A1 = {.menu_type = LIST_STRING,.tab_num = 3,.member = demo_menutab_A1};

// 笔记菜单子内容
    uint8_t teat_data1 = 1, teat_data2 = 0, teat_data3 = 0, teat_data4 = 0;
    kkui_menutab_info_s demo_menutab_A2[]={
        {(char *)"单选1", &teat_data1, SINGLE_CHOICE, func_A2, NULL, &teat_data1},
        {(char *)"单选2", &teat_data2, SINGLE_CHOICE, func_A2, NULL, &teat_data2},
        {(char *)"单选3", &teat_data3, SINGLE_CHOICE, func_A2, NULL, &teat_data3},
        {(char *)"单选4", &teat_data4, SINGLE_CHOICE, func_A2, NULL, &teat_data4},
    };
    kkui_menu_info_s demo_menu_A2 = {.menu_type = LIST_STRING,.tab_num = 4,.member = demo_menutab_A2};

// 无线菜单子内容
                uint8_t esp_now_switch = 0;
                kkui_menutab_info_s demo_menutab_C2[]={
                    {(char *)"开关", &esp_now_switch, SINGLE_CHOICE, NULL, NULL, &esp_now_switch},
                    {(char *)"MAC地址", NULL, ACTION_EXE_ONCE, NULL, NULL, NULL},
                };
                kkui_menu_info_s demo_menu_C2 = {.menu_type = LIST_STRING,.tab_num = 2,.member = demo_menutab_C2};
            typedef struct  {
            int direction;
            int speed;
            uint8_t forw_led;
            uint8_t back_led;
            } recive_info;
            recive_info car_data ={
                .back_led = 0,
                .forw_led = 0,
                .direction = 0,
                .speed = 0,
            };
            kkui_menutab_info_s car_list_menutab[]={
                {(char *)"ESP NOW", NULL, ENTER_LIST, NULL, &demo_menu_C2, NULL},
                {(char *)"前灯", &car_data.forw_led, SINGLE_CHOICE, func_esp_now_car_led, NULL, &car_data.forw_led},
                {(char *)"尾灯", &car_data.back_led, SINGLE_CHOICE, func_esp_now_car_led, NULL, &car_data.back_led},
                {(char *)"指示灯", NULL, ACTION_EXE_ONCE, NULL, NULL, NULL},
                {(char *)"激光", NULL, ACTION_EXE_ONCE, NULL, NULL, NULL},
                {(char *)"舵机", NULL, ACTION_EXE_ONCE, NULL, NULL, NULL},
            };
            kkui_menu_info_s car_list_menu = {.menu_type = LIST_STRING,.tab_num = 6,.member = car_list_menutab};

        kkui_menutab_info_s car_main_menutab[]={
            {NULL, NULL, ENTER_LIST, NULL, &car_list_menu, NULL},
        };
        kkui_menu_info_s car_main_menu = {.menu_type = LIST_WITH_LOOP,.tab_num = 1,.member = car_main_menutab};

        kkui_menutab_info_s tcp_test_menutab[]={
            {(char *)"IP", NULL, ACTION_EXE_LOOP, tcp_client_ip_set, NULL, NULL},
            {(char *)"PORT", NULL, ACTION_EXE_LOOP, tcp_client_port_set, NULL, NULL},
            {(char *)"test", NULL, ACTION_EXE_LOOP, tcp_client_loop, NULL, NULL},
        };
        kkui_menu_info_s tcp_test_menu = {.menu_type = LIST_STRING,.tab_num = 3,.member = tcp_test_menutab};

        kkui_menutab_info_s udp_test_menutab[]={
            {(char *)"IP", NULL, ACTION_EXE_LOOP, udp_client_ip_set, NULL, NULL},
            {(char *)"PORT", NULL, ACTION_EXE_LOOP, udp_client_port_set, NULL, NULL},
            {(char *)"test", NULL, ACTION_EXE_LOOP, udp_client_loop, NULL, NULL},
        };
        kkui_menu_info_s udp_test_menu = {.menu_type = LIST_STRING,.tab_num = 3,.member = udp_test_menutab};

    kkui_menutab_info_s wireless_menutab[]={
        {(char *)"遥控车", NULL, ACTION_EXE_LOOP_WITH_LIST, func_esp_now_car, &car_main_menu, NULL},
        {(char *)"TCP测试", NULL, ENTER_LIST, NULL, &tcp_test_menu, NULL},
        {(char *)"UDP测试", NULL, ENTER_LIST, NULL, &udp_test_menu, NULL},
    };
    kkui_menu_info_s wireless_menu = {.menu_type = LIST_STRING,.tab_num = 3,.member = wireless_menutab};

// 游戏菜单子内容
        extern char snake_mode_name_dis[10];
        extern char snake_dis_buf[2];
        kkui_menutab_info_s snake_set_menutab[] = {
            {(char *)"模式", snake_mode_name_dis, VALUE_SETTINGS, Snake_Set_Mode, NULL, NULL},
            {(char *)"难度", snake_dis_buf, VALUE_SETTINGS, Snake_Set_Level, NULL, NULL},
            {(char *)"关于", NULL, ACTION_EXE_ONCE, NULL, NULL, NULL},
        };
        kkui_menu_info_s snake_set_menu = {.menu_type = LIST_STRING,.tab_num = 3,.member = snake_set_menutab};

        kkui_menutab_info_s snake_game_menutab[] = {
            {NULL, NULL, ACTION_EXE_LOOP, Snake_Game_Loop, NULL, NULL},
            {NULL, NULL, ENTER_LIST, NULL, &snake_set_menu, NULL},
            {NULL, NULL, ACTION_EXE_LOOP, Snake_Record_Loop, NULL, NULL},
        };
        kkui_menu_info_s snake_game_menu = {.menu_type = LIST_WITH_LOOP,.tab_num = 3,.member = snake_game_menutab};

    kkui_menutab_info_s game_menutab[]={
        {(char *)"康威生命游戏", NULL, ACTION_EXE_LOOP, KWG_Loop, NULL, NULL},
        {(char *)"贪吃蛇", NULL, ACTION_EXE_LOOP_WITH_LIST, Snake_Home_Loop, &snake_game_menu, NULL},
    };
    kkui_menu_info_s game_menu = {.menu_type = LIST_STRING,.tab_num = 2,.member = game_menutab};

// wifi菜单子内容
    extern char Wifi_Connect_buf[];
    extern char Wifi_Connect_name[];
    extern uint8_t wifi_onoff;
    kkui_menutab_info_s wifi_menutab[50]={
        {(char *)"WIFI开关", &wifi_onoff, SINGLE_CHOICE, func_wifi_key, NULL, (void *)&wifi_onoff},
        {(char *)Wifi_Connect_buf, (char *)Wifi_Connect_name, ACTION_EXE_ONCE, func_wifi_info, NULL, NULL},
        {(char *)"扫描WIFI", NULL, ACTION_EXE_LOOP, func_wifi_scan, NULL, NULL},
    };
    kkui_menu_info_s wifi_menu = {.menu_type = LIST_STRING,.tab_num = 3,.member = wifi_menutab};

// 设置菜单子内容
    char BRIGHTNESS_BUF[5] = "8";
    static uint8_t dis_mode = 0;
    kkui_menutab_info_s demo_menutab_A5[]={
        {(char *)"亮度", BRIGHTNESS_BUF, VALUE_SETTINGS, func_A51, NULL, NULL},
        {(char *)"反显", &dis_mode, SINGLE_CHOICE, func_A52, NULL, &dis_mode},
        {(char *)"键盘", NULL, ACTION_EXE_LOOP, func_A53, NULL, NULL},
    };
    kkui_menu_info_s demo_menu_A5 = {.menu_type = LIST_STRING,.tab_num = 3,.member = demo_menutab_A5};

// 主菜单内容
kkui_menutab_info_s demo_menutab_main[]={
    {(char *)"波形", (char *)main_icon_pic[0], ENTER_LIST, func_1, &demo_menu_A1, NULL},
    {(char *)"笔记", (char *)main_icon_pic[1], ENTER_LIST, func_2, &demo_menu_A2, NULL},
    {(char *)"串口", (char *)main_icon_pic[3], ACTION_EXE_LOOP, func_loop1, NULL, NULL},
    {(char *)"无线", (char *)main_icon_pic[2], ENTER_LIST, NULL, &wireless_menu, NULL},
    {(char *)"游戏", (char *)main_icon_pic[6], ENTER_LIST, NULL, &game_menu, NULL},
    {(char *)"WIFI", (char *)main_icon_pic[5], ENTER_LIST, NULL, &wifi_menu, NULL},
    {(char *)"设置", (char *)main_icon_pic[4], ENTER_LIST, NULL, &demo_menu_A5, NULL},
};
kkui_menu_info_s demo_menu_main = {.menu_type = LIST_ICON,.tab_num = 7,.member = demo_menutab_main};

// HOME页信息
kkui_menutab_info_s demo_hometab[]={
    {NULL, NULL, ENTER_LIST, func_home, &demo_menu_main, NULL},  // HOME????
};
kkui_menu_info_s menu_home = {.menu_type = LIST_HOME,.tab_num = 1,.member = demo_hometab}; // home的menu_type为LIST_HOME，仅有这个一个为此枚举

kkui_menu_info_s* demo_menu[] = 
{
    &menu_home, // 确保LIST_HOME为第一个
    &demo_menu_main,
    &demo_menu_A1,
    &demo_menu_A2,
    &game_menu,
    &snake_game_menu,
    &snake_set_menu,
    &wireless_menu,
    &car_main_menu,
    &car_list_menu,
    &demo_menu_C2,
    &tcp_test_menu,
    &udp_test_menu,
    &demo_menu_A5,
    &wifi_menu,
};

int demo_menu_num = sizeof(demo_menu)/sizeof(kkui_menu_info_s*);

/* =============================== */
/* function definition*/


void func_home(kkui_menu_event_e event, char* tab_name, void* param)
{
    if(event == LOOP_RUN)
    {
    uiDrawBlock(0, 0, 128 ,64, 0);
    // uiDrawBlock(32, 16, 64 ,32, 2);
    
    // uiDrawCircle(10,16,11,0X0F,2);
    // uiDrawCircle(30,16,2,0X0F,2);
    // uiDrawCircle(50,16,3,0X0F,2);
    // uiDrawCircle(70,16,4,0X0F,2);
    // uiDrawCircle(90,16,6,0X0F,2);
    // uiDrawCircle(110,16,7,0X0F,2);
    
    // uiDrawDisc(10,48,11,0X0F,2);
    // uiDrawDisc(30,48,2,0X0F,2);
    // uiDrawDisc(50,48,3,0X0F,2);
    // uiDrawDisc(70,48,4,0X0F,2);
    // uiDrawDisc(90,48,6,0X0F,2);
    // uiDrawDisc(110,48,7,0X0F,2);

    uiDrawRBlock(30, 30, 20, 20, 5, 2);
    uiDrawRBox(80, 30, 20, 20, 5, 2);
    // uiDrawBox(80, 30, 20, 20, 2);

    uiDrawPoint(32, 32, 0);
    uiDrawPoint(34, 32, 1);
    uiDrawPoint(36, 32, 2);

    
    uiDrawPoint(82, 32, 0);
    uiDrawPoint(84, 32, 1);
    uiDrawPoint(86, 32, 2);
    }
}

void func_1(kkui_menu_event_e event, char* tab_name, void* param)
{
    KKgui_log("func_1 event: %d\r\n", event);
}

void func_2(kkui_menu_event_e event, char* tab_name, void* param)
{
    KKgui_log("func_2 event: %d\r\n", event);
}

void func_3(kkui_menu_event_e event, char* tab_name, void* param)
{
    KKgui_log("func_3 event: %d\r\n", event);
}

void func_4(kkui_menu_event_e event, char* tab_name, void* param)
{
    KKgui_log("func_4 event: %d\r\n", event);
}

void func_A2(kkui_menu_event_e event, char* tab_name, void* param)
{
    KKgui_log("func_A2 event: %d\r\n", event);
    // if(*(uint8_t*)param)
    if(strncmp(tab_name, "单选1", 5) == 0)
    {
        teat_data1 = 1;
        teat_data2 = 0;
        teat_data3 = 0;
        teat_data4 = 0;
    }
    else if(strncmp(tab_name, "单选2", 5) == 0)
    {
        teat_data1 = 0;
        teat_data2 = 1;
        teat_data3 = 0;
        teat_data4 = 0;
    }
    else if(strncmp(tab_name, "单选3", 5) == 0)
    {
        teat_data1 = 0;
        teat_data2 = 0;
        teat_data3 = 1;
        teat_data4 = 0;
    }
    else if(strncmp(tab_name, "单选4", 5) == 0)
    {
        teat_data1 = 0;
        teat_data2 = 0;
        teat_data3 = 0;
        teat_data4 = 1;
    }
}


static uint8_t bright_ness = 8;
static uint8_t l_bright_ness = 8;
void func_A51(kkui_menu_event_e event, char* tab_name, void* param)
{
    if(event == ENTER_TAG)
    {
        l_bright_ness = bright_ness;
    }
    if(event == LOOP_RUN)
    {
        if(uiKeyGetValue(UI_KEY_ENTER_ID) == DOWN || uiKeyGetValue(UI_KEY_SET_ID) == DOWN)
        {
            l_bright_ness = bright_ness;
            uiBackAppLoop();
        }
        if(uiKeyGetValue(UI_KEY_LEFT_ID) == DOWN)
        {
            if(bright_ness > 1) bright_ness --;
        }
        if(uiKeyGetValue(UI_KEY_RIGHT_ID) == DOWN)
        {
            if(bright_ness < 8) bright_ness ++;
        }
        if(uiKeyGetValue(UI_KEY_BACK_ID) == DOWN)
        {
            bright_ness = l_bright_ness;
            uiBackAppLoop();
        }

        oled_setlight((bright_ness - 1) * 32);
        memset(BRIGHTNESS_BUF, 0, 5);
        sprintf(BRIGHTNESS_BUF, "%d", bright_ness);
    }
}


void func_A52(kkui_menu_event_e event, char* tab_name, void* param)
{
    if(event == ENTER_TAG)
    {
        oled_setturn(dis_mode);
    }
}

uint8_t vkey_test_buf[26] = {0};
void func_A53(kkui_menu_event_e event, char* tab_name, void* param)
{
    if(event == ENTER_TAG)
    {
        ;
    }
    if(event == LOOP_RUN)
    {
        uiDisClear(0);
        for(int ret = 0; ret < strlen(vkey_test_buf);)
        {
            uiShowString(1, 10 + 9 * (ret / 21), vkey_test_buf + ret, 8, 1);
            ret += (strlen(vkey_test_buf + ret) > 21?21:strlen(vkey_test_buf + ret));
        }
        if(uiKeyGetValue(UI_KEY_ENTER_ID) == DOWN)
        {
            uiVirtKeyboard(vkey_test_buf, "IN PUT HERE", 25);
        }
        if(uiKeyGetValue(UI_KEY_BACK_ID) == DOWN)
        {
            uiBackAppLoop();
        }
    }
}

void func_loop1(kkui_menu_event_e event, char* tab_name, void* param)
{
    static uint8_t x_cr = 31, y_cr = 13;
    static uint8_t dir_x = 0, dir_y = 0;
    static uint8_t l_state = 0;
    if(l_state != event)
    {
        KKgui_log("func_loop1 event: %d\r\n", event);
        l_state = event;
    }
    if(event == ENTER_TAG)
    {
        uiDisClear(0);
    }
    if(event == LOOP_RUN)
    {
        uiDrawBlock(x_cr, y_cr, 5, 5, 0);

        if(x_cr == 0 || x_cr == 123)
            dir_x = !dir_x;
        if(y_cr == 0 || y_cr == 59)
            dir_y = !dir_y;

        if(dir_x)
        x_cr ++;
            else
        x_cr --;
        if(dir_y)
            y_cr ++;
        else
            y_cr --;
        
        uiDrawBlock(x_cr, y_cr, 5, 5, 1);

        if(uiKeyGetValue(UI_KEY_BACK_ID) == DOWN)
            uiBackAppLoop();
    }
}

void func_esp_now_car(kkui_menu_event_e event, char* tab_name, void* param)
{
    static uint8_t x_cr = 31, y_cr = 13;
    static uint8_t dir_x = 0, dir_y = 0;
    static uint8_t l_state = 0;
    if(l_state != event)
    {
        KKgui_log("func_esp_now_car event: %d\r\n", event);
        l_state = event;
    }
    if(event == ENTER_TAG)
    {
        uiDisClear(0);
        uiDrawBox(30, 15, 15, 34, 1);
        uiDrawBox(83, 15, 15, 34, 1);
        uiDrawBox(50, 22, 28, 20, 1);
    }
    if(event == LOOP_RUN)
    {
        if(uiKeyGetValue(UI_KEY_UP_ID) == DOWN || uiKeyGetValue(UI_KEY_DOWN_ID) == DOWN ||
            uiKeyGetValue(UI_KEY_LEFT_ID) == DOWN || uiKeyGetValue(UI_KEY_RIGHT_ID) == DOWN ||
            uiKeyGetValue(UI_KEY_UP_ID) == UP || uiKeyGetValue(UI_KEY_DOWN_ID) == UP ||
            uiKeyGetValue(UI_KEY_LEFT_ID) == UP || uiKeyGetValue(UI_KEY_RIGHT_ID) == UP ||
            uiKeyGetValue(UI_KEY_ENTER_ID) == DOWN || uiKeyGetValue(UI_KEY_ENTER_ID) == UP)
        {
            if(uiKeyGetValue(UI_KEY_UP_ID) == DOWN)
            {
                car_data.direction = 1;
                car_data.speed = 870;
            }
            if(uiKeyGetValue(UI_KEY_DOWN_ID) == DOWN)
            {
                car_data.direction = 2;
                car_data.speed = 870;
            }
            if(uiKeyGetValue(UI_KEY_LEFT_ID) == DOWN)
            {
                car_data.direction = 3;
                car_data.speed = 870;
            }
            if(uiKeyGetValue(UI_KEY_RIGHT_ID) == DOWN)
            {
                car_data.direction = 4;
                car_data.speed = 870;
            }
            if(uiKeyGetValue(UI_KEY_UP_ID) == UP || uiKeyGetValue(UI_KEY_DOWN_ID) == UP ||
            uiKeyGetValue(UI_KEY_LEFT_ID) == UP || uiKeyGetValue(UI_KEY_RIGHT_ID) == UP)
            {
                car_data.direction = 0;
                car_data.speed = 0;
            }
            if(uiKeyGetValue(UI_KEY_ENTER_ID) == DOWN)
            {
                car_data.forw_led = 1;
            }
            if(uiKeyGetValue(UI_KEY_ENTER_ID) == UP)
            {
                car_data.forw_led = 0;
            }
            esp_now_send_cardata();
        }

        if(uiKeyGetValue(UI_KEY_BACK_ID) == DOWN)
        {
            car_data.direction = 0;
            car_data.speed = 0;
            esp_now_send_cardata();
            uiBackAppLoop();
        }
        if(uiKeyGetValue(UI_KEY_SET_ID) == DOWN)
        {
            car_data.direction = 0;
            car_data.speed = 0;
            esp_now_send_cardata();
            uiEnterAppSubList(0);
        }
        // if(uiKeyGetValue(UI_KEY_LEFT_ID) == DOWN)
        //     uiEnterAppSubList(1);
        // if(uiKeyGetValue(UI_KEY_RIGHT_ID) == DOWN)
        //     uiEnterAppSubList(2);
    }
}


void func_esp_now_car_led(kkui_menu_event_e event, char* tab_name, void* param)
{
    if(event == ENTER_TAG)
    {
        esp_now_send_cardata();
    }
}

// void func_esp_now_car_backled(kkui_menu_event_e event, char* tab_name, void* param)
// {
//     if(event == ENTER_TAG)
//     {
//         esp_now_send_cardata();
//     }
// }


#define     UI_KEY_UP_PIN       2
#define     UI_KEY_DOWN_PIN     0
#define     UI_KEY_LEFT_PIN     1
#define     UI_KEY_RIGHT_PIN    18
#define     UI_KEY_ENTER_PIN    19
#define     UI_KEY_SET_PIN      8
#define     UI_KEY_BACK_PIN     9


uint8_t K_UP_GET(int param)
{
    return (digitalRead(UI_KEY_UP_PIN) == 0);
}
uint8_t K_DOWN_GET(int param)
{
    return (digitalRead(UI_KEY_DOWN_PIN) == 0);
}
uint8_t K_LEFT_GET(int param)
{
    return (digitalRead(UI_KEY_LEFT_PIN) == 0);
}
uint8_t K_RIGHT_GET(int param)
{
    return (digitalRead(UI_KEY_RIGHT_PIN) == 0);
}
uint8_t K_ENTER_GET(int param)
{
    return (digitalRead(UI_KEY_ENTER_PIN) == 0);
}
uint8_t K_SET_GET(int param)
{
    return (digitalRead(UI_KEY_SET_PIN) == 0);
}
uint8_t K_BACK_GET(int param)
{
    return (digitalRead(UI_KEY_BACK_PIN) == 0);
}


kkui_key_cfg_s key_info[] =
{
    1500, NULL, K_UP_GET,
    1500, NULL, K_DOWN_GET,
    1500, NULL, K_LEFT_GET,
    1500, NULL, K_RIGHT_GET,
    1500, NULL, K_ENTER_GET,
    1500, NULL, K_SET_GET,
    1500, NULL, K_BACK_GET
};

