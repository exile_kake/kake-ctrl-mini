#ifndef __KKGUI_VIRT_KEY_H__
#define __KKGUI_VIRT_KEY_H__

/* =============================== */
/* include */
#include "kkgui_cfg.h"

#ifdef __cplusplus
extern "C" {
#endif

/* =============================== */
/* define */


/* =============================== */
/* typedef */



/* =============================== */
/* variable statement*/



/* =============================== */
/* function statement*/
uint8_t uiVirtKeyboard(char *get_data, const char* dis_info, uint16_t max_len);


#ifdef __cplusplus
}
#endif

#endif

