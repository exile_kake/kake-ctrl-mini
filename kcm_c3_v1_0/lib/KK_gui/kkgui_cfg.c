#include "kkgui_cfg.h"

/* =============================== */
/* define */



/* =============================== */
/* typedef */



/* =============================== */
/* variable statement*/
uint8_t     (*KKUI_Gram_Addr)[DIS_WIDE];
int KKUI_Gram_High = 8;
int KKUI_Gram_Wide = 128;

uiRefershCB uiRefresh;


/* =============================== */
/* variable definition*/



/* =============================== */
/* function statement*/



/* =============================== */
/* function definition*/
void uiGramInit(uint8_t ram_addr[][DIS_WIDE])
{    

    KKUI_Gram_Addr = ram_addr;
    KKUI_Gram_Wide = DIS_WIDE;
    KKUI_Gram_High = DIS_HIGH / 8;
}


void uiGramDeinit(void)
{
}


void uiDisInit(uiRefershCB refs_fun)
{
    uiRefresh = refs_fun;
}


