#ifndef __KKGUI_APP_H__
#define __KKGUI_APP_H__


/* =============================== */
/* include */
#include "kkgui_menu.h"
#include "kkgui_cfg.h"


#ifdef __cplusplus
extern "C" {
#endif

/* =============================== */
/* define */

#define     UI_KEY_UP_ID       0
#define     UI_KEY_DOWN_ID     1
#define     UI_KEY_LEFT_ID     2
#define     UI_KEY_RIGHT_ID    3
#define     UI_KEY_ENTER_ID    4
#define     UI_KEY_SET_ID      5
#define     UI_KEY_BACK_ID     6


/* =============================== */
/* typedef */



/* =============================== */
/* variable statement*/

void func_home(kkui_menu_event_e event, char* tab_name, void* param);

void home_func(kkui_menu_event_e event, char* tab_name, void* param);

void func_1(kkui_menu_event_e event, char* tab_name, void* param);
void func_2(kkui_menu_event_e event, char* tab_name, void* param);
void func_3(kkui_menu_event_e event, char* tab_name, void* param);
void func_4(kkui_menu_event_e event, char* tab_name, void* param);
void func_A2(kkui_menu_event_e event, char* tab_name, void* param);
void func_A31(kkui_menu_event_e event, char* tab_name, void* param);
void func_A51(kkui_menu_event_e event, char* tab_name, void* param);
void func_A52(kkui_menu_event_e event, char* tab_name, void* param);
void func_A53(kkui_menu_event_e event, char* tab_name, void* param);

void func_loop1(kkui_menu_event_e event, char* tab_name, void* param);
void func_esp_now_car(kkui_menu_event_e event, char* tab_name, void* param);
void func_esp_now_car_led(kkui_menu_event_e event, char* tab_name, void* param);
// void func_esp_now_car_backled(kkui_menu_event_e event, char* tab_name, void* param);

void func_wifi_key(kkui_menu_event_e event, char* tab_name, void* param);

/* =============================== */
/* function statement*/


#ifdef __cplusplus
}
#endif

#endif

