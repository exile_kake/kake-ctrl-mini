#ifndef __KKGUI_MENU_H__
#define __KKGUI_MENU_H__

#include "kkgui_cfg.h"

#ifdef __cplusplus
extern "C" {
#endif

/* =============================== */
/* define */
#define     PAGE_DISAPP_SPEED           0.25    //  页面消失速度

#define     MENU_LIST_OPEN_PROP_SPEED   0.2    //  列表展开速度
#define     MENU_LIST_OPEN_MAX_SPEED    0.7     //  列表展开最大速度

#define     MENU_LIST_HIGH_PROP_SPEED   0.15     //  光标比例移动速度
#define     MENU_LIST_HIGH_MAX_SPEED    1.8     //  光标移动最大速度

#define     MENU_LIST_WIDTH_PROP_SPEED  0.3     //  光标宽度比例适应速度
#define     MENU_LIST_WIDTH_MAX_SPEED   3.5     //  光标宽度适应最大速度

/* =============================== */
/* typedef */
typedef enum
{
    ENTER_TAG,          // 进入标签事件
    BACK_LIST,          // 退出标签事件
    LOOP_RUN,           // 循环页面事件
}kkui_menu_event_e;

typedef enum
{
    LIST_HOME,           //  启动页面
    LIST_ICON,           
    LIST_STRING,      
    LIST_WITH_LOOP,
}kkui_menu_type_e;

typedef enum // 因为并非各种情况同时使用，应改回非按位模式
{
    ENTER_LIST,                 //  进入下一级列表菜单
    ENTER_LIST_WITH_EXE_LOOP,   //  进入下一级菜单并在菜单期间循环用户函数(未实现)
    VALUE_SETTINGS,             //  数值设置
    SINGLE_CHOICE,              //  单选设置
    ACTION_EXE_ONCE,            //  单次执行用户函数动作
    ACTION_EXE_LOOP,            //  循环执行用户函数动作
    ACTION_EXE_LOOP_WITH_LIST,  //  循环执行用户函数动作且作为子级菜单
    GO_BACK_LIST,               //  单次执行用户函数动作
}kkui_menutab_func_e;

// 标签功能函数，
// 在进入子级标签时会执行一次 event = ENTER_TAG
// 在退出改子级标签时会执行一次 event = BACK_LIST
// 在APP_LOOP类型标签中会先执行一次 event = ENTER_TAG， 然后循环执行event = LOOP_RUN
typedef void (*kkui_menu_func)(kkui_menu_event_e, char*, void*); 
typedef struct kkui_menutab_info_s
{
    char* name;                               // 标签名字
    char* other_dis;                               // 标签图标
    kkui_menutab_func_e type;                       // 标签选项卡功能类型
    kkui_menu_func menu_func;                       // 标签对应子级函数
    struct kkui_menu_info_s* sub_menu_addr;                
    void* param;                                    // 标签参数
    // uint8_t sub_menu_id;                            // 标签对应子级菜单ID
}kkui_menutab_info_s;

typedef struct kkui_menu_info_s
{
    uint8_t menu_id;                // 当前菜单ID，用于判断子关系
    kkui_menu_type_e menu_type;     // 当前列表显示类型
    uint8_t tab_num;                // 当前列表标签数量
    kkui_menutab_info_s* member;    // 标签成员信息
}kkui_menu_info_s; // 菜单配置信息参数

/* =============================== */
/* variable statement*/



/* =============================== */
/* function statement*/

void uiBackAppLoop(void);
void uiEnterAppSubList(uint8_t sub_list_id);

void uiMenuInit(kkui_menu_info_s* uiMenuInfo[], int menu_num);
void uiMenuLoop(void);


#ifdef __cplusplus
}
#endif

#endif

