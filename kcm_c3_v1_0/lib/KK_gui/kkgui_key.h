#ifndef __KKGUI_KEY_H__
#define __KKGUI_KEY_H__

/* =============================== */
/* include */
#include "kkgui_cfg.h"

#ifdef __cplusplus
extern "C" {
#endif

/* =============================== */
/* define */

#define		MAX_KEY_NUM		20

/* =============================== */
/* typedef */

typedef enum 
{
	NONE,
	UP,
	DOWN,
	LONG_PRESS,
	CLICK
}kkui_key_event;


typedef struct 
{
	uint8_t enable; // 使能

	uint32_t longPressTime;

	uint32_t pressTime;
	uint8_t lastPinIO;
	kkui_key_event value;

	void (* KeyEventCallback)(kkui_key_event);
	uint8_t (* KeyGetPinValue)(int);
}kkui_key_info_s;

typedef struct 
{
	uint32_t longPressTime;

	void (* KeyEventCallback)(kkui_key_event);
	uint8_t (* KeyGetPinValue)(int);
}kkui_key_cfg_s;


/* =============================== */
/* variable statement*/



/* =============================== */
/* function statement*/

uint8_t uiKeyCfg(kkui_key_cfg_s cfg);
void uiKeyScan(uint32_t _timeElapseMillis);
void uiKeyClearValue(uint8_t id);
void uiAllKeyClearValue(void);
void uiKeyInit(kkui_key_cfg_s *key_info, uint8_t key_num);
kkui_key_event uiKeyGetValue(uint8_t id);


#ifdef __cplusplus
}
#endif

#endif

