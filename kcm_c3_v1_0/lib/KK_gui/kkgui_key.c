#include "kkgui_key.h"

/* =============================== */
/* define */



/* =============================== */
/* typedef */



/* =============================== */
/* variable statement*/


/* =============================== */
/* variable definition*/

uint32_t key_timer_base;
kkui_key_info_s key_list[MAX_KEY_NUM];

static uint8_t ui_key_num = 0;


/* =============================== */
/* function statement*/



/* =============================== */
/* function definition*/

/* uaer definition*/


/* UI definition*/
uint8_t uiKeyCfg(kkui_key_cfg_s cfg)
{
	uint8_t id;

	id = ui_key_num;
	if(id >= MAX_KEY_NUM)
		return KKUI_ERROR;

	ui_key_num ++;
	
	key_list[id].enable = 1;
	key_list[id].KeyEventCallback = cfg.KeyEventCallback;
	key_list[id].KeyGetPinValue = cfg.KeyGetPinValue;
	key_list[id].longPressTime = cfg.longPressTime;
	key_list[id].pressTime = 0;
	key_list[id].lastPinIO = key_list[id].KeyGetPinValue(id);
    key_list[id].value = NONE;

    return id;
}

void uiKeyScan(uint32_t _timeElapseMillis)
{
	uint8_t id;
	uint8_t pinIO;
    key_timer_base += _timeElapseMillis;
	
	for(id = 0 ; id < ui_key_num ; id ++)
	{
		if(key_list[id].enable == 0)
			continue;
        pinIO = key_list[id].KeyGetPinValue(id);
        if(pinIO == KKUI_ERROR)  
			continue;

        if (key_list[id].lastPinIO != pinIO)
        {
            if(pinIO)
            {
                key_list[id].value = DOWN;
                if(key_list[id].KeyEventCallback)
                    key_list[id].KeyEventCallback(DOWN);
                key_list[id].pressTime = key_timer_base;
            }
            else
            {
                key_list[id].value = UP;
                if(key_list[id].KeyEventCallback)
                    key_list[id].KeyEventCallback(UP);
                // if (key_timer_base - key_list[id].pressTime > key_list[id].longPressTime)
                // {
                //     key_list[id].value = LONG_PRESS;
                //     if(key_list[id].KeyEventCallback)
                //         key_list[id].KeyEventCallback(LONG_PRESS);
                // }
                // else
                // {
                //     key_list[id].value = CLICK;
                //     if(key_list[id].KeyEventCallback)
                //         key_list[id].KeyEventCallback(CLICK);
                // }
            } 

            key_list[id].lastPinIO = pinIO;
            // KKgui_log("key change %d = %d\r\n", id, pinIO);
            // KKgui_log("key value %d = %d\r\n", id, key_list[id].value);
        }
	}
}

void uiKeyClearValue(uint8_t id)
{
    if(id < MAX_KEY_NUM) 
        key_list[id].value = NONE;
}

void uiAllKeyClearValue(void)
{
	uint8_t id;
    for(id = 0 ; id < ui_key_num ; id ++)
	{
        key_list[id].value = NONE;
    }
}

kkui_key_event uiKeyGetValue(uint8_t id)
{
    return key_list[id].value;
}

void uiKeyInit(kkui_key_cfg_s *key_info, uint8_t key_num)
{
    for(uint8_t ret = 0; ret < key_num ; ret ++)
    {
        uiKeyCfg(key_info[ret]);
    }
}

