#include "kkgui.h"

/* =============================== */
/* define */

/* =============================== */
/* typedef */



/* =============================== */
/* variable statement*/
extern uiRefershCB uiRefresh;

/* =============================== */
/* variable definition*/



/* =============================== */
/* function statement*/



/* =============================== */
/* function definition*/
// 心跳函数，检测运行事件
void uiHeartBeat(uint8_t ms)
{
    uiKeyScan(ms);
    uiMenuLoop();
    uiAllKeyClearValue();
    uiRefresh();
}

