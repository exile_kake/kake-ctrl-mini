#ifndef __KKGUI_CFG_H__
#define __KKGUI_CFG_H__

/* =============================== */
/* include */
#include "arduino.h"

#include "stdbool.h"
#include "string.h"
#include "math.h"
#include "kkgui_key.h"

#ifdef __cplusplus
extern "C" {
#endif

/* =============================== */
/* user define */

#define     DIS_HIGH      64    // 屏幕尺寸
#define     DIS_WIDE      128

#define     MENU_MENU_FONT_HIGH         12  // 菜单字体高度
#define     MENU_MENU_FONT_WIDTH        6   // 菜单字体宽度
#define     MENU_MENU_CURSOR_SIZE       16  // 菜单光标高度
#define     MENU_MENU_CURSOR_SHIFT      2   // (MENU_MENU_CURSOR_SIZE - MENU_MENU_FONT_HIGH) / 2
#define     MENU_MENU_BOTHSIDE_SHIFT    2   // 前后空余

#define     MENU_MENU_ICON_SIZE         32  // 菜单图标宽度高度

#define     MENU_KEY_UP     (uiKeyGetValue(UI_KEY_UP_ID) == DOWN)       // 功能对应按键
#define     MENU_KEY_DOWN   (uiKeyGetValue(UI_KEY_DOWN_ID) == DOWN)
#define     MENU_KEY_LETF   (uiKeyGetValue(UI_KEY_LEFT_ID) == DOWN)
#define     MENU_KEY_REGHT  (uiKeyGetValue(UI_KEY_RIGHT_ID) == DOWN)
#define     MENU_KEY_ENTER  (uiKeyGetValue(UI_KEY_SET_ID) == DOWN || uiKeyGetValue(UI_KEY_ENTER_ID) == DOWN)
#define     MENU_KEY_BACK   (uiKeyGetValue(UI_KEY_BACK_ID) == DOWN)

#define     MENU_INIT_INTER     0

/* sys define*/

#define     KKUI_ERROR   0XFF
#define     KKUI_NONE    0XFF

/* =============================== */
/* typedef */
// typedef unsigned char       uint8_t;   
// typedef unsigned short      uint16_t;  
// typedef unsigned int        uint32_t;    



typedef void (*uiRefershCB)(void);


/* =============================== */
/* variable statement*/
void oled_setlight(uint8_t i);
void oled_setturn(uint8_t i);

void KKgui_log(const char *fmt, ...);
void esp_now_send_cardata(void);

/* =============================== */
/* function statement*/
void uiGramInit(uint8_t ram_addr[][DIS_WIDE]);
void uiDisInit(uiRefershCB refs_fun);



#ifdef __cplusplus
}
#endif


#endif

