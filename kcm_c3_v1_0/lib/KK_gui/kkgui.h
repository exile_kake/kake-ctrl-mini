#ifndef __KKGUI_H__
#define __KKGUI_H__

/* =============================== */
/* include */

#include "kkgui_cfg.h"

#include "kkgui_app.h"
#include "kkgui_base.h"
#include "kkgui_menu.h"

#ifdef __cplusplus
extern "C" {
#endif

/* =============================== */
/* define */


/* =============================== */
/* typedef */

/* =============================== */
/* variable statement*/


/* =============================== */
/* function statement*/
void uiHeartBeat(uint8_t ms);


#ifdef __cplusplus
}
#endif


#endif

