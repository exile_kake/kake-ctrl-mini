
from socket import *
from threading import Thread


def dealClient(newSocket,cliAddr):
    
    print('link %s' % (str(cliAddr)))
    while True:
        recvData = newSocket.recv(1024)
        if len(recvData):
            print('rec %s : %s' % (str(cliAddr), recvData))
        else:
            #如果数据长度为0 说明客户端断开连接，此时跳出循环关闭套接字
            print('desconnect%s' % (str(cliAddr)))
            break
    newSocket.close()
    
    
    
def main():
    tcpSocket = socket(AF_INET,SOCK_STREAM)

    #设置套接字可以地址重用
    tcpSocket.setsockopt(SOL_SOCKET, SO_REUSEADDR, True)
    serverAddr = ('',8888)
    tcpSocket.bind(serverAddr)
    tcpSocket.listen(128)
    #设置监听队列长度，在linux中这个值没有太大意义，kernel有自己的值
    
    #为了防止服务端异常退出，因此使用try...finally,确保创建的套接字可以正常关闭
    try:
        while True:
            # 在线程中不断接收新的连接请求
            newSocket,cliAddr = tcpSocket.accept()
            #创建子线程处理已经建立好的连接，tcpSocket依旧去监听是否有新的请求。
            t1 = Thread(target=dealClient,args=(newSocket,cliAddr))
            t1.start()
    
    except:
        pass
    finally:
        tcpSocket.close()
    
    
if __name__ == '__main__':
    main()

