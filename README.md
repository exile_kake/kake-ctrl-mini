<!--
 * @Author: wangtianyu wangtianyu@lierda.com
 * @Date: 2023-06-07 18:16:14
 * @LastEditors: wangtianyu wangtianyu@lierda.com
 * @LastEditTime: 2023-11-20 09:58:31
 * @FilePath: \kake-ctrl-mini\README.md
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
-->
# kake ctrl mini

#### 待完成
0. --图标模式显示菜单（已完成）--
1. --列表右侧位置光标显示（已完成）--
2. 列表切出（返回）函数，选项卡内退出时执行（待验证）
3. --单选框（开关量）实现，带参数，（已完成）--
4. 弹窗切换数字，横向切换，带参数（待定）
5. u8g3基础功能移植
6. 菜单配置信息，动画速度等信息可修改
7. --子子级，即APP_LOOP进入子菜单（uiEnterList函数的应用）（已完成）--
8. --param参数和回调函数参数（已完成）--
9. --列表循环功能（已完成）--
10. 弹窗功能
11. wifi功能，列表扫描
12. 单机：游戏贪吃蛇，数字华容道，翻转小游戏
13. 网游：斗地主
14. 无线遥控，TCP控制
15. 无线，udp流控，投屏
16. home界面设计